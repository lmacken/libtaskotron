==========================
Libtaskotron Documentation
==========================

What is libtaskotron?
=====================

Libtaskotron is one of the core components which make up the `Taskotron`_
system. Libtaskotron is responsible for running tasks written in
:ref:`taskotron-formula-format`.

While libtaskotron was designed for use with `Fedora <http://fedoraproject.org>`_,
the Fedora specific parts are isolated and the core should be usable with any
recent Linux distribution.

Libtaskotron and Taskotron should be considered Alpha software. They are
currently under very heavy development and will likely change until we stabilize
the interfaces.

Contents
--------

.. toctree::
   :maxdepth: 1

   quickstart
   writingtasks
   taskyaml
   library
   devguide
   directives/list_of_directives_modules
   tapformat



.. _what-can-i-do-with-taskotron:

What Can I Do With Taskotron?
=============================

In theory, there is little that cannot be done in Taskotron; by design, it is
a system and framework for running semi-arbitrary tasks. That being said, there
is a limit to what we currently support. If there is some feature or language
that you'd like to see added to Taskotron, let us know through :ref:`contact-us`
or filing a feature request (:ref:`taskotron-bugs`).

Task Languages
--------------

Currently, the only language supported for tasks is Python. However, Taskotron
is designed to work on text-based interactions between the runner and other
units of work. Once the runner matures more, support for additional languages
may be added.

Directives
----------

A Taskotron task is made up of :ref:`taskotron-directives` which control what
can be done in a task. See the list of available :ref:`taskotron-directives`
to learn more about what is currently available.


.. _install-libtaskotron:

Install Libtaskotron
====================

In order to run any tasks locally, libtaskotron must first be installed. While
not currently in the main Fedora repositories, it is available in a side repo
which is updated as new versions are released. To enable this repo::

  sudo curl https://copr-fe.cloud.fedoraproject.org/coprs/tflink/taskotron/repo/fedora-21/tflink-taskotron-fedora-21.repo \
  -o /etc/yum.repos.d/tflink-taskotron-fedora-21.repo

.. TODO - replace this link with a non-copr repo that's more stable over time

Once the repository is enabled, install libtaskotron with::

  sudo yum install libtaskotron

Note: The permissions on several notable directories (e.g. ``/var/log/taskotron``,
``/var/lib/taskotron/``, ``/var/cache/taskotron``) are set to ``777``. If
desired, edit ``/etc/taskotron/taskotron.yaml`` and set these paths to
directories with more appropriate permissions.

.. _running-tasks:

Running Tasks
=============

Using `rpmlint <https://bitbucket.org/fedoraqa/task-rpmlint>`_ as an example,
the task repository contains the following::

  task-rpmlint/
      readme.rst
      rpmlint.py
      rpmlint.yml

Rpmlint runs off of koji builds, so to run it against the x86_64 build of
``foo-1.2-3.fc99`` (substitute a real build if you're going to run this)::

  runtask -i foo-1.2-3.fc99 -t koji_build -a x86_64 rpmlint.yml

The build will be downloaded to a local tempdir, rpmlint will be run against
that build and the results will be printed to the console in TAP13 format.


.. _contact-us:

Contact Us
==========
Please direct questions and comments to either the `Fedora QA Devel List
<https://admin.fedoraproject.org/mailman/listinfo/qa-devel>`_ or the *#fedora-qa*
IRC channel on `Freenode <http://freenode.net/>`_.



.. _Taskotron: https://fedoraproject.org/wiki/Taskotron


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

=====================
Taskotron Quick Start
=====================


Add repo
========

::

  sudo curl https://copr-fe.cloud.fedoraproject.org/coprs/tflink/taskotron/repo/fedora-21/tflink-taskotron-fedora-21.repo \
  -o /etc/yum.repos.d/tflink-taskotron-fedora-21.repo


Install
=======

::

  sudo yum install libtaskotron

`Detailed install instructions <index.html#install-libtaskotron>`_


Run
===

::

  runtask -i <item> -t <type> -a <arch> <yaml recipe>

`More details on running tasks <index.html#running-tasks>`_


Formulae: General Syntax
========================

.. code-block:: yaml

   name: polyjuice potion
   desc: potion that allows to take the form of someone else
   maintainer: hermione

   input:
       args:
           - arg1   # supported args:
           - arg2   # arch, koji_build, bodhi_id, koji_tag

   environment:
       rpm:
           - dependency1  # not checked in current version
           - dependency2  # only for human reference

   actions:
       - name: pick fluxweed on a full moon
         directivename:              # e.g. koji, bodhi, python, ...
             arg1: value1
             arg2: ${some_variable}  # e.g. input arg
         export: firststep_output    # export usable as input arg of next directive

       - name: next step of formula
         nextdirectivename:
          arg1: ${firststep_output}

`Detailed instructions on writing tasks <writingtasks.html#creating-a-new-task>`_


Recipes: Examples From Our Git
==============================

Running Rpmlint
---------------

`Rpmlint <https://bitbucket.org/fedoraqa/task-rpmlint>`_

::

  git clone https://bitbucket.org/fedoraqa/task-rpmlint.git

::

  runtask -i gourmet-0.16.0-2.fc20 -t koji_build -a x86_64 task-rpmlint/rpmlint.yml


Other Examples
--------------


`Rpmbuild <https://bitbucket.org/fedoraqa/task-rpmbuild>`_

`Example bodhi <https://bitbucket.org/fedoraqa/task-examplebodhi>`_

`Example reporting <https://bitbucket.org/fedoraqa/task-example_reporting>`_

Check `our git <https://bitbucket.org/fedoraqa>`_ for more.

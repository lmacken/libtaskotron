============
libtaskotron
============

libtaskotron is a library for running automated tasks as part of the
`Taskotron <https://fedoraproject.org/wiki/Taskotron>`_ system. It is in very
early stages of development with the initial objective of (but not limited to)
replacing AutoQA for automating selected package checks in Fedora.

Libtaskotron should be considered as very early alpha code - the features
contained are generally funcitonal but we will likely continue to make major
changes in how libtaskotron works.

Please direct questions and comments to either *#fedora-qa* on freenode or the
`qa-devel mailing list <https://admin.fedoraproject.org/mailman/listinfo/qa-devel>`_.

For more information, please reference the online documentation:

  * `Development Version <http://libtaskotron.readthedocs.org/en/develop/>`_

  * `Latest Release <http://libtaskotron.readthedocs.org/en/latest/>`_


Installing a Development Environment
====================================

Please consider whether you really need a libtaskotron development
environment. Maybe you simply want to develop tests *using* libtaskotron? In
that case, please follow `libtaskotron install instructions
<https://docs.qadevel.cloud.fedoraproject.org/libtaskotron/latest/#install-libtaskotron>`_
instead. If you really want to develop libtaskotron itself, please continue.

For the moment, libtaskotron can't be fully installed by either pip or rpm and
needs a bit of both for now.

Some of the packages required for using libtaskotron are not yet available from
standard Fedora repos and the taskotron-copr repo is required::

  sudo curl https://copr-fe.cloud.fedoraproject.org/coprs/tflink/taskotron/repo/fedora-21/tflink-taskotron-fedora-21.repo \
  -o /etc/yum.repos.d/tflink-taskotron-fedora-21.repo

Once the copr repo is available, install the necessary packages::

  sudo yum install       \
  createrepo             \
  gcc                    \
  git                    \
  koji                   \
  libtaskotron-config    \
  mash                   \
  pyOpenSSL              \
  python-doit            \
  python-hawkey          \
  python-pip             \
  python-pycurl          \
  python-rpmfluff        \
  python-urlgrabber      \
  python-virtualenv      \
  rpm-python

If you have not yet cloned the repository, do it now::

  git clone https://bitbucket.org/fedoraqa/libtaskotron
  cd libtaskotron

Then, set up the virtualenv::

  virtualenv --system-site-packages env_taskotron
  source env_taskotron/bin/activate
  pip install -r requirements.txt

Finally, you should install libtaskotron, so that you don't need to specify the
full path for the taskotron runner every time you want to run it. You can either
use a standard installation, if you want to just use this project::

  pip install .

Or, if you intend to work on this project, you can install it in the editable
mode. This way you don't need to reinstall the project every time you make some
changes to it, the code changes are reflected immediately::

  pip install -e .

Before running any task, you also need to manually create a few required
directories::

  sudo mkdir -m 777 /var/tmp/tasktron /var/log/taskotron /var/cache/taskotron \
                    /var/lib/taskotron /var/lib/taskotron/artifacts


Configuration
=============

The ``libtaskotron-config`` package installs config files with default values
into ``/etc/taskotron``. If you need to change those default values, you can
either change the files in ``/etc/taskotron`` or you can create config files
inside your checkout with::

  cp conf/taskotron.yaml.example conf/taskotron.yaml
  cp conf/yumrepoinfo.conf.example conf/yumrepoinfo.conf

The configuration files in ``conf/`` take precedence over anything in ``/etc``,
so make sure that you're editing the correct file if you create local copies.

In the development environment, it's also useful to have taskotron-generated
files automatically cleaned up, so that they don't occupy disk space in vain.
There is a tmpfiles.d template prepared for you, look into ``conf/tmpfiles.d``.


Running a Task
==============

A relatively simple example task is `rpmlint <https://bitbucket.org/fedoraqa/task-rpmlint>`_.

The task requires the rpmlint tool to be installed, so be sure to run::

  sudo yum install rpmlint

To run that task against a koji build with NEVR ``<nevr>`` for some arch
``<arch>``, do the following::

  git clone https://bitbucket.org/fedoraqa/task-rpmlint.git
  runtask -i <nevr> -t koji_build -a <arch> task-rpmlint/rpmlint.yml

This will download the ``<nevr>`` from koji into a temp directory under
``/var/tmp/``, run rpmlint on the downloaded rpms and print output in TAP format
to stdout.

Example::

  runtask -i xchat-2.8.8-21.fc20 -t koji_build -a x86_64 task-rpmlint/rpmlint.yml


Running the Test Suite
======================

You can run the included test suite of unit and functional tests.
From the root checkout directory, execute::

  py.test testing/

to run unit tests, or::

  py.test --functional testing/

to run both unit and functional tests (recommended). Make sure to use the
virtualenv-provided version of py.test, not the system one, otherwise you'll
encounter import errors.

You can also see a test coverage of the code, just run::

  py.test --functional --cov-report term-missing --cov libtaskotron testing/

A nice HTML-based representation is available if you use ``--cov-report html``
parameter instead.

If you write new tests, be sure to run this to see whether the code is
sufficiently covered by your tests.


Building Documentation
======================

Libtaskotron's documentation is written in `reStructuredText
<http://docutils.sourceforge.net/rst.html>`_ and built using `Sphinx
<http://sphinx-doc.org/>`_.

The documentation is easy to build if you have followed the instructions to set
up a development environment.

To actually build the documentation::

  doit builddocs

Build Automation
================

There are several development related tasks which are at least somewhat
automated using `doit <http://pydoit.org/>`_ which has repaced our old Makefile.

After either installing the doit package (``python-doit``, ``python3-doit``) or
via pip (``pip install doit``), you can see a list of available tasks and a short
description of those tasks by running ``doit list``. Some of the available tasks
are:

  * ``buildsrpm`` takes a snapshot of current git repo and uses the in-repo spec
    file to build a matching srpm in the ``builds/<version>`` directory. Note
    that if a snapshot already exists for a given version, a new snapshot will
    not be generated until the existing one is deleted.

  * ``chainbuild`` uses mockchain and the existing COPR repo to build a noarch
    binary rpm from the latest srpm.

  * ``builddocs`` builds documentation from current git sources

By default, the tool is pretty quiet but if you would like to see more verbose
output, add ``--verbosity 2`` to the doit command and all stdout/stderr output
will be shown.

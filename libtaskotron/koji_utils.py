# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

''' Utility methods related to Koji '''

from __future__ import absolute_import
import os

import collections
import koji
import hawkey

from libtaskotron import file_utils
from libtaskotron.logger import log
from libtaskotron import exceptions as exc
from libtaskotron import rpm_utils
from libtaskotron import config


class KojiClient(object):
    '''Helper Koji methods.

    :ivar koji.ClientSession session: Koji client session
    '''

    def __init__(self, koji_session=None):
        '''Create a new KojiClient

        :param koji_session: an existing Koji session instance or ``None`` if
                             you want a new default session to be created
        :type koji_session: :class:`koji.ClientSession`
        '''
        opts = {
            # make koji retry even for non-logged-in connections.
            # retry behavior can be configured via 'max_retries' (default 30)
            # and 'retry_interval' (default 20) opts keys
            'anon_retry': True,
            # default Koji connection timeout is 12 hours, that's too high.
            # let's make it 5 minutes
            'timeout': 60 * 5}

        self.session = (koji_session or
                        koji.ClientSession(config.get_config().koji_url, opts=opts))

    def latest_by_tag(self, tags, pkgname):
        '''Get the latest Koji build for the given package name in the given tag(s).

        :param list tags: list of tags to be searched in
        :param str pkgname: name of the package
        :return: str containing NVR of the latest build, None if no build was found
        '''
        self.session.multicall = True
        for tag in tags:
            self.session.listTagged(tag, package=pkgname, latest=True)
        builds = self.session.multiCall()

        nvrs = []
        for build in builds:
            # see rpms_to_build for documentation on how multiCall works
            if isinstance(build, dict):
                raise exc.TaskotronRemoteError('listTagged failed with: %d: %s' %
                                               (build["faultCode"], build["faultString"]))
            elif build[0]:
                assert len(build[0]) <= 1, 'More than one build returned with latest=True'
                nvrs.append(build[0][0]['nvr'])

        sorted_nvrs = sorted(nvrs, cmp=rpm_utils.cmpNEVR, reverse=True)
        if sorted_nvrs:
            return sorted_nvrs[0]

    def rpms_to_build(self, rpms):
        '''Get list of koji build objects for the rpms. Order of koji objects
        in this list is the same as order of the respective rpm objects.

        :param rpms: list of filenames as either ``/my/path/nvr.a.rpm`` or ``nvr.a.rpm``
        :type rpms: list of str
        :return: list of Koji buildinfo dictionaries (as returned e.g.
          from :meth:`koji.getBuild`) in the same respective order as in``rpms``
        :rtype: list
        :raise TaskotronRemoteError: if rpm or it's related build is not found
        '''
        log.info('Querying Koji to map %d RPMs to their builds...', len(rpms))

        self.session.multicall = True
        for rpm in rpms:
            # extract filename from the (possible) path
            rpm = os.path.split(rpm)[-1]

            self.session.getRPM(rpm)

        # the list will contain one element for each method added to the
        # multicall, in the order it was added to the multicall
        rpminfos = self.session.multiCall()

        # because it is probable that several rpms will come from the same build
        # use set for builds
        builds = set()
        for i, rpminfo in enumerate(rpminfos):
            # according to documentation, multiCall() returns list, where
            # each element will be either a one-element list containing the
            # result of the method call, or a dict containing "faultCode" and
            # "faultString" keys, describing the error that occurred during the
            # method call.
            #
            # getRPM() returns None if there is no RPM with the given ID
            if isinstance(rpminfo, dict):
                raise exc.TaskotronRemoteError('Problem with RPM %s: %d: %s' % (
                    rpms[i], rpminfo["faultCode"], rpminfo["faultString"]))
            elif rpminfo[0] is None:
                raise exc.TaskotronRemoteError('RPM %s not found' % rpms[i])
            else:
                builds.add(rpminfo[0]['build_id'])

        builds = list(builds)  # so that we could use build order
        self.session.multicall = True
        for build in builds:
            self.session.getBuild(build)

        buildinfos = self.session.multiCall()

        for i, buildinfo in enumerate(buildinfos):
            # see ^
            if isinstance(buildinfo, dict):
                raise exc.TaskotronRemoteError('Problem with build %s: %d: %s' %
                                               (builds[i], buildinfo["faultCode"],
                                                buildinfo["faultString"]))
            elif buildinfo[0] is None:
                raise exc.TaskotronRemoteError(
                    'Build %s not found' % builds[i])

        build_to_buildinfo = dict(zip(builds, buildinfos))
        result = []
        for rpminfo in rpminfos:
            build_id = rpminfo[0]['build_id']
            result.append(build_to_buildinfo[build_id][0])

        return result

    def nvr_to_urls(self, nvr, arches=['all'], debuginfo=False, src=True):
        '''Get list of URLs for RPMs corresponding to a build.

        :param str nvr: build NVR
        :param arches: restrict the arches of builds to provide URLs for. By
            default, all architectures are considered. If you want to consider
            just some selected arches, provide their names in a list.

            .. note:: If basearch ``i386`` is in the list, ``i686`` arch is
              automatically added (since that's the arch Koji API uses).
        :type arches: list of str
        :param bool debuginfo: whether to provide URLs for debuginfo RPM files
            or ignore them
        :param bool src: whether to include a URL for the source RPM
        :rtype: list of str
        :raise TaskotronRemoteError: when the requested build doesn't exist
        :raise TaskotronValueError: if ``arches=[]`` and ``src=False``,
            therefore there is nothing to query for
        '''
        log.info('Querying Koji for a list of RPMS for: %s', nvr)

        # add i686 arch if i386 is present in arches
        if 'i386' in arches and 'i686' not in arches:
            arches.append('i686')

        # if src is enabled, we need to add it so that it is included in the
        # Koji query
        if src and ('src' not in arches):
            arches.append('src')

        # if "nothing" is requested, it's probably an error
        if not arches:
            raise exc.TaskotronValueError('Nothing to query for, `arches` was set to an empty '
                                          'list and `src` was disabled. At least one of them '
                                          'need to be non-empty.')

        # find the koji build
        info = self.session.getBuild(nvr)
        if info is None:
            raise exc.TaskotronRemoteError("No such build found in Koji: %s" % nvr)

        # list its RPM files
        req_arches = None if 'all' in arches else arches
        rpms = self.session.listRPMs(buildID=info['id'], arches=req_arches)
        if not debuginfo:
            rpms = [r for r in rpms if (not r['name'].endswith('-debuginfo'))
                    and ('-debuginfo-' not in r['name'])]
        if not src:
            rpms = [r for r in rpms if not r['arch'] == 'src']

        # create URLs
        baseurl = '/'.join((config.get_config().pkg_url, info['package_name'],
                            info['version'], info['release']))
        urls = ['%s/%s' % (baseurl, koji.pathinfo.rpm(r)) for r in rpms]

        return sorted(urls)

    def get_nvr_rpms(self, nvr, dest, arches=['all'], debuginfo=False, src=False):
        '''Retrieve the RPMs associated with a build NVR into the specified
        directory.

        :param str nvr: build NVR
        :param str dest: location where to store the RPMs
        :param arches: see :meth:`nvr_to_urls`
        :param debuginfo: see :meth:`nvr_to_urls`
        :param src: see :meth:`nvr_to_urls`
        :return: list of local filenames of the grabbed RPMs (might be empty,
            according to your option choices and the particular NVR)
        :rtype: list of str
        :raise TaskotronRemoteError: if the files can't be downloaded
        :raise TaskotronValueError: if ``arches=[]`` and ``src=False``,
            therefore there is nothing to download
        '''
        # always create dest dir, even if nothing gets downloaded
        file_utils.makedirs(dest)

        rpm_urls = self.nvr_to_urls(nvr, arches, debuginfo, src)

        rpm_files = []
        log.info('Fetching %s RPMs for: %s (into %s)', len(rpm_urls), nvr, dest)

        # RPMs are safe to be cached, Koji guarantees they are immutable
        cachedir = None
        if config.get_config().download_cache_enabled:
            cachedir = config.get_config().cachedir

        for url in rpm_urls:
            rpm_file = file_utils.download(url, dest, cachedir=cachedir)
            rpm_files.append(rpm_file)

        return rpm_files

    def get_tagged_rpms(self, tag, dest, arches=['all'], debuginfo=False, src=False):
        '''Downloads all RPMs of all NVRs tagged by a specific Koji tag.

        Note: This works basically the same as :meth:`get_nvr_rpms`, it just
        downloads a lot of builds instead of a single one. For description of
        all shared parameters and return values, please see that method.

        :param str tag: Koji tag to be queried for available builds, e.g.
                        ``f20-updates-pending``
        '''
        # always create dest dir, even if nothing gets downloaded
        file_utils.makedirs(dest)

        log.info('Querying Koji for tag: %s', tag)
        tag_data = self.session.listTagged(tag)

        nvrs = sorted([x['nvr'] for x in tag_data])
        rpms = []
        log.info("Fetching %s builds for tag: %s", len(nvrs), tag)
        log.debug('Builds to be downloaded:\n  %s', '\n  '.join(nvrs))

        for nvr in nvrs:
            rpms.extend(self.get_nvr_rpms(nvr, dest, arches, debuginfo, src))

        return rpms


def getNEVR(build):
    '''Extract RPM version identifier in NEVR format from Koji build object

    :param dict build: Koji buildinfo dictionary (as returned e.g. from
                       :meth:`koji.getBuild`)
    :return: NEVR string; epoch is included when non-zero, otherwise omitted
    :raise TaskotronValueError: if ``build`` is of incorrect type
    '''
    # validate input
    if (not isinstance(build, collections.Mapping) or 'nvr' not in build or
            'epoch' not in build):
        raise exc.TaskotronValueError("Input argument doesn't look like "
                                      "a Koji build object: %s" % build)

    rpmver = hawkey.split_nevra(build['nvr'] + '.noarch')
    rpmver.epoch = build['epoch'] or 0
    nevr = '%s-%s' % (rpmver.name, rpmver.evr())
    # supress 0 epoch (evr() method always includes epoch, even if 0)
    nevr = rpm_utils.rpmformat(nevr, fmt='nevr')

    return nevr

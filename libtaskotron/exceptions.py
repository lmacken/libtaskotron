# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

'''This module contains custom Taskotron exceptions'''

from __future__ import absolute_import

class TaskotronError(Exception):
    '''Common ancestor for Taskotron related exceptions'''
    pass


class TaskotronValueError(ValueError, TaskotronError):
    '''Taskotron-specific :class:`ValueError`'''
    pass


class TaskotronConfigError(TaskotronError):
    '''All errors related to Taskotron config files'''
    pass

class TaskotronYamlError(TaskotronError):
    '''Error in YAML config file of the executed check'''
    pass


class TaskotronDirectiveError(TaskotronError):
    '''All errors related to Taskotron directives'''
    pass


class TaskotronRemoteError(TaskotronError):
    '''All network and remote-server related errors'''
    pass

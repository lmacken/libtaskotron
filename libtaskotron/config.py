# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

'''Global configuration for Taskotron and relevant helper methods.'''

from __future__ import absolute_import
import os
import yaml
import collections

import libtaskotron
from libtaskotron import exceptions as exc
from libtaskotron.logger import log
from libtaskotron.config_defaults import (Config, ProductionConfig,
                                          TestingConfig, ProfileName)
from libtaskotron import file_utils

CONF_DIRS = [ # local checkout dir first, then system wide dir
    os.path.abspath(os.path.dirname(libtaskotron.__file__) + '/../conf'),
    '/etc/taskotron']
'''A list of directories where config files are stored. The config files are
loaded from these locations in the specified order, and only the first config
file found is used (further locations are ignored). The first location is
dynamic, relative to the package location and it reflects the usual config
location in a git checkout.'''

CONF_FILE = 'taskotron.yaml'
'''the name of our configuration file'''

PROFILE_VAR = 'TASKOTRON_PROFILE'
'''environment variable name for setting config profile'''

_config = None
'''a singleton instance of Config'''


def get_config():
    '''Get the Config instance. This method is implemented using the singleton
    pattern - you will always receive the same instance, which will get
    auto-initialized on the first method call.

    :return: either :class:`.Config` or its subclass, depending on taskotron
             profile used.
    :raise TaskotronConfigError: if config file parsing and handling failed
    '''

    global _config
    if not _config:
        _config = _get_instance()
    return _config


def _get_instance():
    '''Do everything necessary to fully initialize Config instance, update it
    with external configuration, make all final touches.

    :return: :class:`.Config` instance ready to be used
    :raise TaskotronConfigError: if file config parsing and handling failed
    '''
    config = _load()
    log.debug('Using config profile: %s', config.profile)

    # make sure required directories exist
    _create_dirs(config)

    return config


def _load():
    '''Load the configuration, internal (defaults) and external (config files)

    :return: :class:`.Config` instance that was updated by external config file
             contents
    :raise TaskotronConfigError: if file config parsing and handling failed
    '''

    # first load the defaults and make sure we even want to load config files
    env_profile = os.getenv(PROFILE_VAR)
    config = _load_defaults(env_profile)
    if config.profile == ProfileName.TESTING:
        log.debug('Testing profile, not loading config files from disk')
        return config

    # load config files
    filename = _search_dirs(CONF_DIRS, CONF_FILE)
    if not filename:
        log.warn('No config file %s found in dirs: %s' % (CONF_FILE, CONF_DIRS))
        return config

    log.debug('Using config file: %s', filename)
    file_config = _load_file(filename)
    file_profile = file_config.get('profile', None)

    # reload the config defaults if the profile was specified in the config file
    # (and not present in env variable)
    if file_profile and not env_profile:
        config = _load_defaults(file_profile)

    # merge the changes from the config file
    _merge_config(config, file_config)

    # set config filename used, this is set after merging
    # so it doesn't get overridden
    config.config_filename = filename

    return config


def _load_defaults(profile):
    '''Load and return Config (or its subclass) based on chosen profile.

    :param str profile: profile name as defined in :class:`.ProfileName`
    :return: :class:`.Config` instance or its subclass
    '''

    if profile == ProfileName.PRODUCTION:
        return ProductionConfig()
    elif profile == ProfileName.TESTING:
        return TestingConfig()
    else:
        if profile and profile != ProfileName.DEVELOPMENT:
            log.warn('Unknown profile "%s", selecting "%s"', profile,
                     ProfileName.DEVELOPMENT)
        # the default is the development profile
        return Config()


def _search_dirs(conf_dirs, conf_file):
    '''Find the configuration file in a set of directories. The first match
    is returned.

    :param conf_dirs: a list of directories to search through
    :type conf_dirs: list of str
    :param str conf_file: configuration file name
    :return: full path to the configuration file (string) or ``None`` if not
             found
    '''

    for conf_dir in conf_dirs:
        filename = os.path.join(conf_dir, conf_file)
        if os.access(filename, os.R_OK):
            return filename


def _load_file(conf_file):
    '''Parse a configuration file and return it as a dictionary. The option
    values are checked for type correctness against a default Config object.

    :param conf_file: file path (string) or file handler to the configuration
                      file in YAML syntax
    :return: dictionary parsed from the configuration file
    :raise TaskotronConfigError: if any problem occurs during the parsing or
                                 some values have incorrect variable type
    '''

    # convert file path to file handle if needed
    if isinstance(conf_file, basestring):
        try:
            conf_file = open(conf_file)
        except IOError as e:
            log.exception('Could not open config file: %s', conf_file)
            raise exc.TaskotronConfigError(e)

    filename = (conf_file.name if hasattr(conf_file, 'name') else
                '<unnamed file>')
    try:
        conf_obj = yaml.safe_load(conf_file)
    except yaml.YAMLError as e:
        log.exception('Could not parse config file: %s', filename)
        raise exc.TaskotronConfigError(e)

    # config file might be empty (all commented out), returning None. For
    # further processing, let's replace it with empty dict
    if conf_obj is None:
        conf_obj = {}

    # check correct types
    # we should receive a single dictionary with keyvals
    if not isinstance(conf_obj, collections.Mapping):
        raise exc.TaskotronConfigError('The config file %s does not have '
        'a valid structure. Instead of a mapping, it is recognized as: %s' %
        (filename, type(conf_obj)))

    default_conf = Config()
    for option, value in conf_obj.items():
        # check for unknown options
        try:
            default_value = getattr(default_conf, option)
        except AttributeError:
            log.warn('Unknown option "%s" in the config file %s', option,
                     filename)
            continue

        # check for correct type
        assert default_value is not None, \
            "Default values must not be None: %s" % option
        if type(default_value) is not type(value):
            raise exc.TaskotronConfigError('Option "%s" in config file %s '
                'has an invalid type. Expected: %s, Found: %s'
                % (option, filename, type(default_value), type(value)))

    return conf_obj


def _merge_config(config, file_config):
    '''Merge a Config object and a dictionary parsed from YAML file.

    :param config: a :class:`.Config` instance to merge into
    :param dict file_config: a dictionary parsed from YAML configuration file to
                             merge from
    :return: the same ``config`` instance, but with some attributes overwritten
             with values from ``file_config``
    :raise AttributeError: if ``file_config`` contains values that ``config``
                           doesn't have. But this shouldn't happen, because
                           :func:`_load_file` should already have checked for
                           that.
    '''

    for option, value in file_config.items():
        if option == 'profile':
            # the only option not overridden from file is 'profile', because
            # that might have been dictated by env variable which has a higher
            # priority
            continue
        if hasattr(config, option):
            setattr(config, option, value)


def _create_dirs(config):
    '''Create directories in the local file system for appropriate config
    options, like ``tmpdir``, ``logdir``, ``cachedir``, ``artifactsdir``.

    :param config: :class:`.Config` instance
    :raise TaskotronError: when directories don't exist and can't be created
    '''

    for dir_ in (config.tmpdir, config.logdir, config.artifactsdir,
                 config.cachedir):
        try:
            file_utils.makedirs(dir_)
        except OSError as e:
            raise exc.TaskotronError("Failed to create required directory '%s', "
                                     "please create it manually. Caused by: %s"
                                     % (dir_, e))

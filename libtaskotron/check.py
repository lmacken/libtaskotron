# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

'''Helper tools for managing check status, outcome, and output.'''

from __future__ import absolute_import
import pprint
import collections

import tap

from . import python_utils
from . import exceptions as exc
from .logger import log

from pytap13 import TAP13

# a list of reserved keywords for TAP output, which can't be overridden in
# keyvals
RESERVED_KEYS = ('item', 'type', 'outcome',
                 'summary', 'details', 'checkname',
                 'artifact', '_internal')

class CheckDetail(object):
    '''Class encompassing everything related to the outcome of a check run.
    It contains convenience methods for tracking, evaluating and reporting
    check results.

    For some checks, it's trivial to parse its output at the end of its
    execution and evaluate the results. For some, however, it's much easier
    to do this continuously during check execution. This is especially true
    for multi-item checks (a single check providing results for many items -
    builds, updates, etc). That's when this class comes very handy (it can be
    used even for the simple use cases, of course).

    :cvar outcome_priority: a tuple of :attr:`outcome` keywords sorted by
                            priority from the least important to the most
                            important
    :ivar str item: a description the item being tested; for example a build NVR
                    (``vpnc-0.5-1.fc20``), update ID (``FEDORA-2014-3309``) or a
                    repository name (``f20-updates``)
    :ivar str report_type: a definition of the type of the object being checked;
                           for example 'a Koji build', 'a Bodhi update' or 'a
                           yum repository name'. The allowed values are
                           attributes in :class:`ReportType`. You don't have to
                           fill this in (or you can provide a custom string
                           value), but the reporting directives react only to
                           the known types (you can always find it in ResultsDB,
                           though).
    :ivar str outcome: a keyword specifying the final outcome of the check.
                       Available outcome keywords:

                       * PASSED - everything went well
                       * INFO - everything went well, but there is some
                         important information that needs to be pointed out
                       * FAILED - the item in question fails the check
                       * NEEDS_INSPECTION - the outcome can't be determined and
                         a manual inspection is needed
                       * ABORTED - the check aborted itself because of some
                         unexpected problems, i.e. a necessary network server is
                         not reachable. Running this check with the same
                         arguments later can help to mitigate this problem.
                       * CRASHED - the check crashed and did not provide usable
                         results

                       If no outcome has been set, this attribute returns
                       NEEDS_INSPECTION.

                       Raises :class:`TaskotronValueError` if you try to assign
                       an unknown keyword.
    :ivar str summary: a few words or one-sentence summary about the result of
                       the check run (e.g. ``5 WARNINGS, 1 ERROR`` for an
                       rpmlint result). Should not unnecessarily duplicate
                       :attr:`item` or :attr:`outcome`, if possible.
    :ivar output: output from the check run (a list of strings). You can easily
                  populate this by using @method store(), or you can modify it
                  directly (for example filter out some messages you don't want
                  to see in the check output).
    :ivar dict keyvals: all key-value pairs in this dictionary are stored in
                        ResultsDB as 'extra data'. This can be used to store
                        e.g. a compose id or a kernel version, if that
                        information is important for querying the results. Keep
                        it as short as possible.
    :ivar str checkname: name of the check, which the CheckDetail belongs to. This
                         is usually not needed, as the check name is devised from
                         the task metadata, but if a single task produces
                         results for multiple checks, this is the way to override
                         the default behavior.
    :ivar str artifact: path to a file or directory placed in the artifacts directory,
                        either absolute ``$artifactsdir/report.html`` or relative
                        ``report.html``. It will represent task output specific for
                        this particular :attr:``item``.
    '''

    outcome_priority = ('PASSED', 'INFO', 'FAILED', 'NEEDS_INSPECTION',
                        'ABORTED', 'CRASHED')


    def __init__(self, item, report_type=None, outcome=None, summary='',
                 output=None, keyvals=None, checkname=None, artifact=None):
        # validate input
        if (output is not None and
            not python_utils.sequence(output, basestring, mutable=True)):
            raise exc.TaskotronValueError("'output' parameter must be a "
                "mutable sequence of strings. Yours was: %s" % type(output))
        if keyvals is not None and not isinstance(keyvals, collections.Mapping):
            raise exc.TaskotronValueError("'keyvals' parameter must be a "
                    "mapping. Yours was: %s" % type(keyvals))

        self.item = item
        self.report_type = report_type
        self._outcome = None
        if outcome:
            self.outcome = outcome
        self.checkname = checkname
        self.summary = summary
        self.output = output or []
        self.keyvals = keyvals or {}
        self.artifact = artifact
        # this dictionary will hold implementation-specific data, that needs
        # to be transfered in between directives (like resultsdb_result_id)
        self._internal = {}


    @property
    def outcome(self):
        return self._outcome or 'NEEDS_INSPECTION'


    @outcome.setter
    def outcome(self, value):
        if value not in self.outcome_priority:
            raise exc.TaskotronValueError('Unknown outcome keyword: %s' % value)
        self._outcome = value


    def update_outcome(self, outcome):
        '''Update :attr:`outcome` with the provided value only and only if it
        has a higher priority than its current value (according to
        :attr:`outcome_priority`). Otherwise this call does nothing.

        This is useful if your check performs a number of 'sub-tasks', each
        passing or failing, and you want to final outcome to be the worst/most
        serious one of those. You can just keep calling :meth:`update_outcome`
        and the highest priority outcome type will be stored at :attr:`outcome`
        at the end.

        :param str outcome: the outcome value to assign to :attr:`outcome` if it
                            has a higher priority than its current value.
                            Handles ``None`` values gracefully (no action).
        :raise TaskotronValueError: if any of the outcome keywords are not
                                    specified in :attr:`outcome_priority`'''
        if self.cmp_outcome(outcome, self._outcome) > 0:
            self.outcome = outcome


    def broken(self):
        '''Tells whether the check :attr:`outcome` is set to one of the broken
        states (i.e. ABORTED or CRASHED).'''
        return self.outcome in ['ABORTED', 'CRASHED']


    def store(self, message, printout=True):
        '''Add a string to the :attr:`output`, and print it optionally as well.

        This is just a convenience method for most common use case, you can
        of course always access and modify :attr:`output` directly, and print or
        use logging facilities directly as well. This combines both into a
        single call.

        :param str message: a string to store in :attr:`output`
        :param bool printout: whether to print to standard output or not
        '''

        self.output.append(message)
        if printout:
            print message


    @classmethod
    def cmp_outcome(cls, outcome1, outcome2):
        '''Compare two outcomes according to :attr:`outcome_priority` and return
        -1/0/1 if ``outcome1`` has lower/equal/higher priority than ``outcome2``.

        :param str outcome1: an outcome keyword to compare
        :param str outcome2: an outcome keyword to compare
        :raise TaskotronValueError: if any of the outcome keywords are not
                                    specified in :attr:`outcome_priority`
        '''

        # validate input
        for outcome in [outcome1, outcome2]:
            if (outcome not in cls.outcome_priority) and (outcome is not None):
                raise exc.TaskotronValueError('Unknown outcome keyword: %s' %
                                              outcome)

        index1 = cls.outcome_priority.index(outcome1) if outcome1 is not None \
                                                      else -1
        index2 = cls.outcome_priority.index(outcome2) if outcome2 is not None \
                                                      else -1
        return cmp(index1, index2)


    @classmethod
    def create_multi_item_summary(cls, outcomes):
        '''Create a string containing a sum of all outcomes, like this:
        ``3 PASSED, 1 INFO, 2 FAILED``

        :param outcomes: either one :class:`CheckDetail` instance or an iterable
                          of :class:`CheckDetails <CheckDetail>` or an iterable
                          of :attr:`outcome` strings
        '''
        # validate input
        if isinstance(outcomes, CheckDetail):
            outcomes = (outcomes,)

        if len(outcomes) <= 0:
            return ''

        if python_utils.iterable(outcomes, CheckDetail):
            all_outcomes = [detail.outcome for detail in outcomes]
        else:  # list of strings
            if not python_utils.iterable(outcomes, basestring):
                raise exc.TaskotronValueError("'outcomes' parameter type is "
                    'incorrect: %s' % type(outcomes))
            all_outcomes = outcomes

        # create the summary
        summary = []
        for res in cls.outcome_priority:
            count = all_outcomes.count(res)
            if count > 0:
                summary.append('%d %s' % (count, res))
        summary = ', '.join(summary)

        return summary


    def __str__(self):
        # Make this object more readable when printing
        # But don't show self.output, because that can be huuge
        attrs = vars(self)
        attrs['output'] = '<stripped out>'
        return '<%s: %s>' % (self.__class__.__name__, pprint.pformat(attrs))


class ReportType(object):
    ''' Enum for different types of :attr:`CheckDetail.report_type`'''
    # the values are used as identifiers in a TAP export
    KOJI_BUILD = 'koji_build'          #:
    BODHI_UPDATE = 'bodhi_update'      #:
    YUM_REPOSITORY = 'yum_repository'  #:
    COMPOSE = 'compose'  #:


def export_TAP(check_details, checkname="$CHECKNAME"):
    '''Generate TAP output used for reporting to ResultsDB.

    Note: You need to provide all your :class:`CheckDetail`\s in a single pass
    in order to generate a valid TAP output. You can't call this method several
    times and then simply join the outputs simply as strings.

    :param check_details: iterable of :class:`CheckDetail` instances or single
                          instance of :class:`CheckDetail`
    :param str checkname: name of the executed check
    :return: TAP output with results for every :class:`CheckDetail` instance
             provided
    :rtype: str
    :raise TaskotronValueError: if :attr:`CheckDetail.item` is empty for any
                                parameter provided

    Example output::

      TAP Version 13
      1..1
      ok - $CHECKNAME for Koji build xchat-0.5-1.fc20
        ---
        details:
          output: |-
            xchat.x86_64: W: file-not-utf8 /usr/share/doc/xchat/ChangeLog
            xchat.x86_64: W: non-conffile-in-etc /etc/gconf/schemas/apps.schemas
            xchat.x86_64: W: no-manual-page-for-binary xchat
        item: xchat-0.5-1.fc20
        outcome: PASSED
        summary: 5 ERRORS, 10 WARNINGS
        type: koji_build
        artifact: xchat-0.5-1.fc20.x86_64.log
        ...
    '''

    # if check_details is single CheckDetail create a list with one element
    if isinstance(check_details, CheckDetail):
        check_details = [check_details]


    # validate input
    for detail in check_details:
        if not detail.item:
            raise exc.TaskotronValueError('CheckDetail.item is empty for: %s' %
                                          detail)

    # TAP test line will be more readable for recognized report types
    pretty_type_name = {
        ReportType.KOJI_BUILD: 'Koji build',
        ReportType.BODHI_UPDATE: 'Bodhi update',
        ReportType.YUM_REPOSITORY: 'Yum repository',
    }

    tapgen = tap.TAPGenerator()
    tapout = ['TAP version 13', '1..%d' % len(check_details)]

    for detail in check_details:
        # TAP test line
        tap_outcome = tap.FAIL
        if detail.outcome in ['PASSED', 'INFO']:
            tap_outcome = tap.PASS

        name = '%s for ' % checkname
        if detail.report_type:
            name += '%s ' % pretty_type_name.get(detail.report_type,
                                                 detail.report_type)
        name += detail.item

        # TAP YAML block
        data = {}
        data['outcome'] = detail.outcome
        data['item'] = detail.item
        if detail.artifact:
            data['artifact'] = detail.artifact
        if detail.report_type:
            data['type'] = detail.report_type
        if detail.summary:
            data['summary'] = detail.summary
        if detail.checkname:
            data['checkname'] = detail.checkname
        data['details'] = {}
        if detail._internal:
            data['_internal'] = detail._internal
        if detail.output:
            data['details']['output'] = '\n'.join(detail.output)
        if not data['details']:
            del data['details']

        for key, value in detail.keyvals.iteritems():
            if key in RESERVED_KEYS:
                log.warn("Reserved key '%s' found in keyvals. Ignoring for "
                         "export purposes.", key)
                continue

            data[key] = value

        # generate
        output = tapgen.format_TAP_msg(result=tap_outcome, name=name, data=data)
        tapout.append(output)

    return '\n'.join(tapout)

def import_TAP(source):
    '''Parses TAP and returns a list of :class:`CheckDetails <CheckDetail>`.

    :param str source: TAP-formatted text
    :raise TaskotronValueError: if TAP syntax is incorrect
    '''

    tap = TAP13()
    try:
        tap.parse(source)
    except ValueError as e:
        raise exc.TaskotronValueError('Failed to parse TAP contents: %s' % e)

    check_details = []
    for test in tap.tests:
        y = test.yaml or {}
        item = y.get('item', None)
        report_type = y.get('type', None)
        artifact = y.get('artifact', None)
        outcome = 'PASSED' if test.result == 'ok' else 'FAILED'
        outcome = y.get('outcome', outcome)
        summary = y.get('summary', '')
        checkname = y.get('checkname', None)
        details = y.get('details', {})
        output = details.get('output', None)
        _internal = y.get('_internal', {})
        if output is not None:
            output = [output]

        # TODO: is there a better way to do this?
        other_keys = set(y.keys()) - set(RESERVED_KEYS)
        keyvals = dict([(k,y[k]) for k in other_keys])

        cd = CheckDetail(item, report_type, outcome, summary, output, checkname=checkname, artifact=artifact)

        cd.keyvals = keyvals
        cd._internal = _internal
        check_details.append(cd)

    return check_details


# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

''' Configure logging in libtaskotron.

There are two modes how to operate - as an external library, or as the main
script runner:

* In the external library mode, we try not to change any global defaults, not
  touch the root logger, and not attach any handlers. The main script author
  should be in control of all these things.
* In the main script runner mode, we control everything - we configure the root
  logger, attach handlers to it, and set verbosity of this and any other library
  as we see fit.
'''

from __future__ import absolute_import
import sys
import os
import tempfile
import logging
import logging.handlers
import traceback
import time
# you must not import libtaskotron.config here because of cyclic dependencies

# http://docs.python.org/2/howto/logging.html#configuring-logging-for-a-library
#: the main logger for libtaskotron library, easily accessible from all our
#: modules
log = logging.getLogger('libtaskotron')
log.addHandler(logging.NullHandler())  # this is needed when running in library mode

# log formatting
_fmt_full = '[%(name)s:%(filename)s:%(lineno)d] '\
       '%(asctime)s %(levelname)-7s %(message)s'
_fmt_simple = '[%(name)s] %(asctime)s %(levelname)-7s %(message)s'
_datefmt_full = '%Y-%m-%d %H:%M:%S'
_datefmt_simple = '%H:%M:%S'
_formatter_full = logging.Formatter(fmt=_fmt_full, datefmt=_datefmt_full)
_formatter_simple = logging.Formatter(fmt=_fmt_simple, datefmt=_datefmt_simple)
# set logging time to UTC/GMT
_formatter_full.converter = time.gmtime
_formatter_simple.converter = time.gmtime

#: our current stream handler sending logged messages to stderr
stream_handler = None

#: our current syslog handler sending logged messages to syslog
syslog_handler = None

#: our current file handler sending logged messages to file log
file_handler = None

#: our current memory handler sending logged messages to memory
#: log prior to creating file log (after its creation, content
#: of the memory log is flushed into the file log)
mem_handler = None


def _create_handlers(syslog=False, filelog_path=None):
    '''Create all handlers. This should be called before any method tries to
    operate on handlers. Handlers are created only if they don't exist yet
    (they are `None`), otherwise they are skipped. So this method can easily be
    called multiple times.

    Note: Handlers can't be created during module import, because that breaks
    stream capturing functionality when running through ``pytest``.

    :param bool syslog: create syslog handler. Syslog must be available.
    :param str filelog_path: path to the log file where :data:`file_handler`
                             should emit messages. If this is ``None``, then
                             :data:`file_handler` is not created.
    :raise socket.error: if syslog handler creation failed
    '''
    global stream_handler, syslog_handler, file_handler, mem_handler

    if not stream_handler:
        stream_handler = logging.StreamHandler()
        stream_handler.setFormatter(_formatter_simple)

    if syslog and not syslog_handler:
        syslog_handler = logging.handlers.SysLogHandler(address='/dev/log',
                             facility=logging.handlers.SysLogHandler.LOG_LOCAL4)

    if not file_handler and filelog_path:
        file_handler = logging.FileHandler(filelog_path, encoding='UTF-8')
        file_handler.setFormatter(_formatter_full)

    if not mem_handler:
        mem_handler = logging.handlers.MemoryHandler(capacity=1024)
        mem_handler.setFormatter(_formatter_full)


def _log_excepthook(*exc_info):
    '''Called when an exception is not caught'''
    log.critical(''.join(traceback.format_exception(*exc_info)))


def _set_verbosity_levels():
    '''Configure libtaskotron and other important libraries for maximum desired
    verbosity. The actual verbosity is then adjusted in our handlers. This
    should be only called when running as the main script.'''
    log.setLevel(logging.DEBUG)
    logging.getLogger('resultsdb_api').setLevel(logging.DEBUG)
    # TODO: set this to INFO once this is resolved:
    # https://fedorahosted.org/koji/ticket/317
    logging.getLogger('koji').setLevel(logging.DEBUG)


def init_prior_config(level_stream=None):
    '''Initialize Taskotron logging with default values which do not rely on
    a config file. Only stream logging is enabled here. This is used before the
    config file is loaded. After that a proper initialization should take place
    through the :func:`init` method.

    Note: Since this touches the root logger, it should be called only when
    Taskotron is run as the main program (through its runner), not when it is
    used as a library.

    :param int level_stream: message level of the stream logger. The level
        definitions are in :mod:`logging`. If ``None``, the default level is
        used (i.e. :data:`logging.NOTSET`).
    '''
    _create_handlers()
    _set_verbosity_levels()

    if level_stream is not None:
        stream_handler.setLevel(level_stream)

    rootlogger = logging.getLogger()
    rootlogger.addHandler(stream_handler)
    rootlogger.addHandler(mem_handler)

    sys.excepthook = _log_excepthook


def _set_level(handler, level, conf_name):
    '''Set logging level to a handler. Fall back to config defaults if the level
    name is invalid.

    :param handler: log handler to configure
    :type handler: instance of :class:`logging.Handler`
    :param level: level identification from :mod:`logging`
    :type level: ``str`` or ``int``
    :param str conf_name: the name of the configuration option of the default
        level for this handler. If ``level`` value is invalid, it will retrieve
        the default value from the config file and set it instead.
    '''
    from libtaskotron import config
    try:
        handler.setLevel(level)
    except ValueError:
        conf_defaults = config._load_defaults(config.get_config().profile)
        default_level = getattr(conf_defaults, conf_name)
        log.warning("Invalid logging level '%s' for '%s'. Resetting to default "
                    "value '%s'.", level, conf_name, default_level)
        handler.setLevel(default_level)


def init(level_stream=None, level_file=None,
         stream=True, syslog=False, filelog=None,
         filelog_path=None):
    """Initialize Taskotron logging.

    Note: Since this touches the root logger, it should be called only when
    Taskotron is run as the main program (through its runner), not when it is
    used as a library.

    :param int level_stream: level of stream logging as defined in
        :mod:`logging`. If ``None``, a default level from config file is used.
    :param int level_file: level of file logging as defined in
        :mod:`logging`. If ``None``, a default level from config file is used.
    :param bool stream: enable logging to process stream (stderr)
    :param bool syslog: enable logging to syslog
    :param bool filelog: enable logging to a file log. If ``None``, the value is
        loaded from config file.
    :param str filelog_path: path to the log file. If ``None``, the value is
        loaded from config file.
    """

    # We import libtaskotron.config here because import from beginning
    # of this module causes problems with cyclic dependencies
    from libtaskotron import config
    conf = config.get_config()

    if level_stream is None:
        level_stream = conf.log_level_stream
    if level_file is None:
        level_file = conf.log_level_file
    if filelog is None:
        filelog = conf.log_file_enabled
    if filelog_path is None:
        filelog_path = os.path.join(conf.logdir, conf.log_name)

    _create_handlers()
    rootlogger = logging.getLogger()
    sys.excepthook = _log_excepthook

    if stream:
        _set_level(stream_handler, level_stream, "log_level_stream")
        if stream_handler.level <= logging.DEBUG:
            stream_handler.setFormatter(_formatter_full)
        rootlogger.addHandler(stream_handler)
        log.debug("Stream logging enabled with level: %s",
                  logging.getLevelName(stream_handler.level))
    else:
        rootlogger.removeHandler(stream_handler)

    if syslog:
        _create_handlers(syslog=True)
        rootlogger.addHandler(syslog_handler)
        log.debug("Syslog logging enabled with level: %s",
                  logging.getLevelName(syslog_handler.level))
    else:
        rootlogger.removeHandler(syslog_handler)

    if filelog:
        # try access
        try:
            f = open(filelog_path, 'a')
            # since we have the file opened at the moment, put a separator
            # into it, it will help to differentiate between different task runs
            f.write('#'*120 + '\n')
            f.close()
        except IOError as e:
            log.warning("Log file can't be opened for writing: %s\n  %s",
                        filelog_path, e)
            # we will use a temporary log file
            filelog_path = tempfile.mkstemp(suffix='.log', prefix='taskotron-',
                                            dir=conf.tmpdir)[1]
            log.warning("Using a temporary log file instead: %s", filelog_path)
            # update config options so they correspond to reality
            conf.logdir = conf.tmpdir
            conf.log_name = os.path.basename(filelog_path)

        global file_handler
        rootlogger.removeHandler(file_handler)
        file_handler = None
        _create_handlers(filelog_path=filelog_path)
        _set_level(file_handler, level_file, "log_level_file")

        # forward all 'missed' messages from memory to file
        # there's a method for automatically forwarding all messages from a
        #  memory handler, but it does not respect target handler logging level.
        # we need to do it manually
        for record in mem_handler.buffer:
            if record.levelno >= file_handler.level:
                file_handler.handle(record)

        mem_handler.close()
        rootlogger.removeHandler(mem_handler)
        rootlogger.addHandler(file_handler)
        log.debug('File logging enabled with level %s into: %s',
                  logging.getLevelName(file_handler.level), filelog_path)
    else:
        rootlogger.removeHandler(file_handler)
        rootlogger.removeHandler(mem_handler)


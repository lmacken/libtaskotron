# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

from __future__ import absolute_import
import os

class Arches():
    '''
    Helper class for working with supported arches inside taskotron
    '''

    #: all supported architectures
    all = ['i386', 'i486', 'i586', 'i686',
           'x86_64',
           'armhfp', 'arm7hl',
           'noarch', 'src']

    #: base architectures
    base = ['i386', 'x86_64', 'armhfp']

    #: meta architectures
    meta = ['noarch', 'src']

def basearch(arch=None):
    '''
    This returns the 'base' architecture identifier for a specified architecture
    (e.g. ``i386`` for ``i[3-6]86``), to be used by YUM etc.

    :param str arch: an architecture to be converted to a basearch. If ``None``,
                     then the arch of the current machine is used.
    :return: basearch, or ``arch`` if no basearch was found for it
    :rtype: str
    '''
    if arch is None:
        arch = os.uname()[4]
    if arch in ['i386', 'i486', 'i586', 'i686']:
        arch = 'i386'
    if arch in ['armhfp', 'arm7hl']:
        arch = 'armhfp'
    return arch
# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing
from __future__ import absolute_import

DOCUMENTATION = """
module: python_directive
short_description: execute a python script in current process
description: |
  Execute a piece of python code as part of task execution. You can execute any
  callable object imported from your external python file. The object is called
  in the current task process.
parameters:
  file:
    required: true
    description: absolute or relative path to the python file to be executed
      (the task directory is considered the current working directory)
    type: str
  callable:
    required: true
    description: name of the object to call. This can be a function reference,
      a callable instance reference, or an ``instance.method`` reference. See
      examples.
    type: str
  <kwarg>:
    required: false
    description: arbitrarily named parameter with a value. All these ``key=value``
      pairs will be passed to the python script as ``callable`` parameters.
    type: anything
returns: |
  The output of the python script, which must be either ``None`` or a
  ``basestring``.

  If this python script is supposed to be the main check of the task, it is
  supposed to provide output in a `TAP13 format`_. This format is then used in
  the :ref:`resultsdb directive <resultsdb_directive>`. The easiest way to
  create it is to use :class:`.CheckDetail` objects to construct your result (or
  results), and then generate the TAP output with :func:`~.check.export_TAP`.
  Read more in :ref:`writing-tasks-for-taskotron`.

  .. _TAP13 format: http://testanything.org/tap-version-13-specification.html
raises: |
  * :class:`.TaskotronDirectiveError`: if ``file`` can't be read, or if
    ``callable`` returns something else than ``None`` or ``basestring``
  * anything that the python script raises itself
version_added: 0.4
"""

EXAMPLES = """
This runs a rpmlint python-wrapper to process all RPMs in the workdir::

    - name: run rpmlint on downloaded rpms
      python:
          file: run_rpmlint.py
          callable: run
          workdir: ${workdir}
      export: rpmlint_output

You can pass complex python-structures as keyval parameters::

    - name: run upgradepath
      python:
          file: upgradepath.py
          callable: main
          custom_args: [--debug, "${koji_tag}"]
      export: upgradepath_output

.. note:: If you use JSON list syntax (``[a, b, c]``) inside the YAML file, you
  must enclose any variables (and anything else containing curly braces) in
  quotation marks. If your variable is not supposed to be a string, you have to
  use YAML-native list syntax (dashes separated by line breaks). Example follows.

Depcheck uses a lot of keyval parameters, also uses both JSON list syntax and
YAML list syntax, depending on its needs (``${yumrepos}`` is not a string, but a
dictionary)::

    - name: run depcheck
      python:
          file: run_depcheck.py
          callable: taskotron_run
          arch: ${arch}
          rpms: [ "${workdir}/downloaded_tag/" ]
          repos:
              - ${yumrepos}
              - workdir_repo: ${workdir}/downloaded_tag/
          report_format: rpms
      export: depcheck_output

There are several ways how to implement the callable object inside the python
script. Given a callable name ``do_this_thing``, possible implementations could
include:

A) function reference::

    def do_this_thing(some, args):
        return "I'm a function!"

B) instance method reference::

    class TaskClass(object):
        def my_task(self, some, args):
            return "I'm a class method!"

    _task_instance = TaskClass()
    do_this_thing = _task_instance.my_task

C) callable instance reference::

    class EmbeddedCallClass(object):
        def __call__(self, some, args):
            return "I'm a __call__ method in a class!"

    do_this_thing = EmbeddedCallClass()
"""

import imp
import os
import re

from libtaskotron.directives import BaseDirective
from libtaskotron.exceptions import TaskotronDirectiveError
from libtaskotron.logger import log

directive_class = 'PythonDirective'

class PythonDirective(BaseDirective):

    def _do_getattr(self, module, name):
        """Isolation of getattr to make the code more easily testable

        :param module module: module from which to getattr from
        :param str name: name of object to getattr
        :returns: object retrieved from module
        """

        return getattr(module, name)

    def execute(self, task_module, method_name, kwargs):
        """Execute a callable in the specified module

        :param module task_module: module containing the specified callable
        :param str method_name: name of callable to execute
        :param dict kwargs: kwargs to pass as parameters into the callable
        :returns: output from the executed method
        :raise TaskotronDirectiveError: if the callable returns something else
            than ``None`` or ``basestring``
        """

        task_method = self._do_getattr(task_module, method_name)

        module = re.sub('.py[co]?$', '', os.path.basename(task_module.__file__))
        log.info("Executing Python: %s.%s() with args %s", module, method_name,
                 kwargs)
        output = task_method(**kwargs)

        if output is not None and not isinstance(output, basestring):
            raise TaskotronDirectiveError(
                "Callable task method must return string or None, actual "
                "returned type: %s" % type(output))

        return output

    def load_pyfile(self, filename):
        """Import python code specified

        :param str filename: absolute path to the python file
        """

        task_importname = 'runtask_%s' % os.path.basename(filename).rstrip(
                                                                        '.py')
        task = imp.load_source(task_importname, filename)

        return task_importname, task

    def checkfile(self, filename):
        """Check to see if the file exists

        :param str filename: abspath to the filename
        :raises TaskotronDirectiveError: if the file does not exist
        """

        if not os.path.exists(filename):
            raise TaskotronDirectiveError("The file %s does not exist!"
                                          % filename)

    def process(self, input_data, env_data):
        """Process this directive.
        :param dict input_data: dictionary of all kwargs from yaml declaration,
            including callable and file
        :param dict env_data: information on the environment in which we're
            running
        :returns: output from the callable specified, this is expected to be TAP
            output for checks
        :raise TaskotronDirectiveError: if the callable returns something else
            than ``None`` or ``basestring``
        """

        if 'callable' not in input_data or 'file' not in input_data:
            detected_args = ', '.join(input_data.keys())
            raise TaskotronDirectiveError(
                "The python directive requires both 'callable' and 'file' "
                "arguments. Detected arguments: %s" % detected_args)

        method_name = input_data.pop('callable')
        basedir = os.path.dirname(env_data['taskfile'])

        pyfile = os.path.join(basedir, input_data.pop('file'))

        self.checkfile(pyfile)

        _, task = self.load_pyfile(pyfile)

        output = self.execute(task, method_name, input_data)

        return output

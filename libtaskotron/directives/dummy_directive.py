# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing
from __future__ import absolute_import

DOCUMENTATION = """
module: dummy_directive
short_description: test how directives work
description: |
  This is just what it sounds like - a directive that doesn't do anything other
  than (optionally) return a message or raise an error. It is primarially meant
  for testing the runner or as a placeholder while writing new tasks. Or you can
  play with it to learn the directive basics.
parameters:
  result:
    required: true
    description: arbitrary string. If it equals ``FAIL`` (case ignored), then
      the directive raises an error.
    type: str
  msg:
    required: false
    description: arbitrary string. If it is present, it is returned.
    type: anything
returns: |
  Either ``msg``, if it was provided, or ``None``.
raises: |
  * :class:`.TaskotronDirectiveError`: if ``result="FAIL"`` (case ignored)
version_added: 0.4
"""

EXAMPLES = """
This simply returns ``msg`` that was provided::

 - name: return Koji build (NVR)
   dummy:
     result: PASS
     msg: ${koji_build}

This raises an error, regardless of whether or what was provided in ``msg``::

 - name: raise an error
   dummy:
     result: FAIL
"""


from libtaskotron.directives import BaseDirective
from libtaskotron.exceptions import TaskotronDirectiveError

directive_class = 'DummyDirective'

class DummyDirective(BaseDirective):

    def process(self, input_data, env_data):
        expected_result = input_data['result']

        if expected_result.lower() == 'fail':
            raise TaskotronDirectiveError("This is a dummy directive and "
                                          "configured to fail")

        if 'msg' in input_data:
            return input_data['msg']

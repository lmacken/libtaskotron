# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing
from __future__ import absolute_import

DOCUMENTATION = """
module: resultsdb_directive
short_description: send task results to ResultsDB or check TAP correctness
description: |
  Send check output to `ResultsDB <https://fedoraproject.org/wiki/ResultsDB>`_.

  If reporting to ResultsDB is disabled in the config (`report_to_resultsdb`
  option, disabled by default for the development profile), the directive at
  least checks whether the check output is in valid `TAP13 format`_ and logs the
  details that would have been reported. For this reason, it's useful to use
  resultsdb directive always, even when you don't have any ResultsDB server
  configured.

  .. _TAP13 format: http://testanything.org/tap-version-13-specification.html
parameters:
  results:
    required: true
    description: The output of the check in TAP13 format
    type: str
returns: |
  TAP13 formatted output as taken from input, enhanced with result-id's for each
  successfully stored result. The result-id information then can be used by
  other directives - e.g. ``bodhi_comment_directive``. If reporting to ResultsDB
  is disabled, the returned TAP does not contain this extra field.
raises: |
  * :class:`.TaskotronDirectiveError`: if ``results`` is not in a valid TAP13
    format, or when  ``item`` or ``type`` are missing in the TAP's result data
  * :class:`resultsdb_api.ResultsDBapiException`: for any errors coming from
    the ResultsDB server
version_added: 0.4
"""

EXAMPLES = """
These two actions first run ``run_rpmlint.py`` and export its TAP13 output to
``${rpmlint_output}``, and then feed this TAP13 output to ``resultsdb``
directive::

    - name: run rpmlint on downloaded rpms
      python:
          file: run_rpmlint.py
          callable: run
          workdir: ${workdir}
      export: rpmlint_output

    - name: report results to resultsdb
      resultsdb:
          results: ${rpmlint_output}
      export: resultsdb_output

If ResultsDB reporting is configured in the config file, it will get saved on
the ResultsDB server, otherwise only TAP13 compliance will get checked and some
summary will be printed out into the log, like this::

    [libtaskotron:resultsdb_directive.py:143] 2014-06-24 13:55:27 INFO    TAP is OK.
    [libtaskotron:resultsdb_directive.py:144] 2014-06-24 13:55:27 INFO    Reporting to ResultsDB is disabled.
    [libtaskotron:resultsdb_directive.py:145] 2014-06-24 13:55:27 INFO    Once enabled, the following would be reported:
    [libtaskotron:resultsdb_directive.py:146] 2014-06-24 13:55:27 INFO    <CheckDetail: {'_outcome': 'PASSED',
     'item': 'xchat-2.8.8-21.fc20.x86_64.rpm',
     'keyvals': {},
     'output': '<stripped out>',
     'report_type': 'koji_build',
     'summary': 'RPMLINT PASSED for xchat-2.8.8-21.fc20.x86_64.rpm'}>
    <CheckDetail: {'_outcome': 'PASSED',
     'item': 'xchat-tcl-2.8.8-21.fc20.x86_64.rpm',
     'keyvals': {},
     'output': '<stripped out>',
     'report_type': 'koji_build',
     'summary': 'RPMLINT PASSED for xchat-tcl-2.8.8-21.fc20.x86_64.rpm'}>
"""

import os

from libtaskotron.directives import BaseDirective

from libtaskotron import check
from libtaskotron import config
from libtaskotron import buildbot_utils

from libtaskotron.exceptions import TaskotronDirectiveError, TaskotronValueError
from libtaskotron.logger import log

import resultsdb_api

directive_class = 'ResultsdbDirective'

class ResultsdbDirective(BaseDirective):

    def __init__(self, resultsdb = None):
        self.resultsdb = resultsdb

        conf = config.get_config()
        self.masterurl = conf.taskotron_master
        self.task_stepname = conf.buildbot_task_step
        self.execdb_server = "%s/jobs" % conf.execdb_server
        self.artifacts_baseurl = conf.artifacts_baseurl

        if self.resultsdb is None:
            self.resultsdb = resultsdb_api.ResultsDBapi(conf.resultsdb_server)

        self._ensured_testcases = []

    def ensure_testcase_exists(self, name):
        """ Make sure that the testcase exists in resultsdb, otherwise create
        the testcase using a dummy url as a reference

        :param str name: name of the testcase to check for
        """

        if name in self._ensured_testcases:
            return

        try:
            self.resultsdb.get_testcase(name)
            self._ensured_testcases.append(name)
            return
        except resultsdb_api.ResultsDBapiException, e:
            if not e.message.startswith('Testcase not found'):
                raise e

        # since the testcase doesn't exist, create it with a dummy value for url
        # it can be updated later when it's not blocking results reporting
        dummyurl = 'http://faketestcasesRus.com/%s' % name
        self.resultsdb.create_testcase(name, dummyurl)
        self._ensured_testcases.append(name)

    def create_resultsdb_job(self, name, uuid=None, refurl=None):
        """ Create a job in resultsdb for reporting results against

        :param str name: name of job to report against
        :param str uuid: UUID of the job (most probably provided by ExecDB)
        :param str refurl: url pointing to the execution overview.
           If set to None, ExecDB url is created from UUID
        :returns: dict of rendered json results
        """

        url = "%s/%s" % (self.execdb_server, uuid)
        if refurl is not None:
            url = refurl

        jobdata = self.resultsdb.create_job(url, status='SCHEDULED', name=name,
                                            uuid=uuid)
        self.resultsdb.update_job(id=jobdata['id'], status='RUNNING')

        return jobdata

    def complete_resultsdb_job(self, jobid):
        """ Change the resultsdb job to a status of ``COMPLETED``, indicating
        that the reporting is complete.

        :param int jobid: The resultsdb jobid to be modified
        :returns: json representation of returned data from the resultsdb instance
        """
        return self.resultsdb.update_job(jobid, status='COMPLETED')

    def get_artifact_path(self, artifactsdir, artifact):
        """Return the relative path of :attr str artifact: inside the
        :attr str artifactsdir:.
        :returns: relative path to the artifact file or None, if the file
                  does not exist, or is outside the artifactsdir.
        """
        artifactsdir = os.path.realpath(artifactsdir)
        if os.path.isabs(artifact):
            artifact_path = artifact
        else:
            artifact_path = os.path.join(artifactsdir, artifact)
        artifact_path = os.path.realpath(artifact_path)

        if not os.path.exists(artifact_path):
            log.warn('Artifact %r does not exist, ignoring' % artifact_path)
            return None
        elif not artifact_path.startswith(artifactsdir):
            log.warn('Artifact %r is placed outside of artifacts directory %r, ignoring',
                     artifact_path, artifactsdir)
            return None

        return os.path.relpath(artifact_path, start=artifactsdir)

    def process(self, input_data, env_data):
        #TODO: Replace with proper location and adjust exc. text
        # we're creating the jobid in the directive for now, so adjust the check

        if 'checkname' not in env_data:
            raise TaskotronDirectiveError("The resultsdb directive requires "\
                    "resultsdb_job_id and checkname.")

        # checking if reporting is enabled is done after importing tap which
        # serves as validation of input results
        try:
            check_details = check.import_TAP(input_data['results'])
            log.debug("TAP output parsed OK.")
        except TaskotronValueError as e:
            raise TaskotronDirectiveError("Failed to load 'results': %s"  % e.message)

        for detail in check_details:
            if not (detail.item and detail.report_type):
                raise TaskotronDirectiveError("The resultsdb directive requires "
                        "'item' and 'type' to be present in the TAP data.")

        conf = config.get_config()
        if not (conf.reporting_enabled and conf.report_to_resultsdb):
            log.info("Reporting to ResultsDB is disabled. Once enabled, the "
                     "following would get reported:\n%s" %
                     report_summary(input_data['results'], env_data['checkname']))
            log.info('Hint: Enabling debug output allows you to see unstripped '
                     'values during variable export.')
            return check.export_TAP(check_details)

        # for now, we're creating the resultsdb job at reporting time
        # the job needs to be 'RUNNING' in order to append any results
        job_url, log_url = buildbot_utils.get_urls(env_data['jobid'],
                                                   self.masterurl,
                                                   self.task_stepname)
        job_data = self.create_resultsdb_job(env_data['checkname'],
                                             uuid=env_data['uuid'],
                                             )

        log.info('Posting %s results to ResultsDB...' % len(check_details))
        for detail in check_details:
            checkname = detail.checkname or env_data['checkname']
            self.ensure_testcase_exists(checkname)
            result_log_url = log_url
            if detail.artifact:
                artifact_path = self.get_artifact_path(
                    env_data['artifactsdir'],
                    detail.artifact
                    )
                if artifact_path:
                    result_log_url = "%s/all/%s/task_output/%s" % (
                        self.artifacts_baseurl,
                        env_data['uuid'],
                        artifact_path,
                        )
            try:
                result = self.resultsdb.create_result(
                        job_id = job_data['id'],
                        testcase_name = checkname,
                        outcome = detail.outcome,
                        summary = detail.summary or None,
                        log_url=result_log_url,
                        item = detail.item,
                        type = detail.report_type,
                        **detail.keyvals)
                detail._internal['resultsdb_result_id'] = result['id']

            except resultsdb_api.ResultsDBapiException, e:
                log.error(e)
                log.error("Failed to store ResulsDB: `%s` `%s` `%s`",
                           detail.item, checkname, detail.outcome)

        # move job to completed
        self.complete_resultsdb_job(job_data['id'])

        return check.export_TAP(check_details)


def report_summary(tap, checkname):
    '''Create a pretty summary of what will/would be reported into ResultsDB.
    The summary is based on TAP output, with the TAP header stripped out and
    most of the check output stripped out.

    :param str tap: TAP-formatted results
    :param str checkname: the name of the check
    :return: summary in plain text
    :rtype: str
    :raise TaskotronValueError: if `tap` has invalid format
    '''

    check_details = check.import_TAP(tap)
    summary = []

    for detail in check_details:
        # keep just the first and the last line of output
        lines = ('\n'.join(detail.output)).splitlines()
        if len(lines) > 3:
            detail.output = [lines[0],
                             '<... %s lines stripped out ...>' % (len(lines) - 2),
                             lines[-1]]
        out = check.export_TAP(detail, checkname=checkname)
        # strip "TAP version 13\n1..1" (first two lines)
        out = '\n'.join(out.splitlines()[2:])
        summary.append(out)

    return '\n'.join(summary)

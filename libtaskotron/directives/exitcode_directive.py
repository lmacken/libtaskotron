# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

from libtaskotron.directives import BaseDirective
from libtaskotron.exceptions import TaskotronDirectiveError
from libtaskotron.logger import log
from libtaskotron import check


DOCUMENTATION = """
module: exitcode_directive
short_description: set runtask exit code based on last or worst TAP outcome
description: |
  This directives takes TAP specified by key ``tap_last`` or ``tap_worst`` (keys are mutually
  exclusive), and generates returncode based on last or worst TAP outcome. If TAP is empty,
  exitcode is set to SUCCESS.

  If task formula contains multiple usages of exitcode directive, worst exitcode is returned by
  runtask.

  Input for directive is supposed to be in a `TAP13 format`_. The easiest way to create it is to
  use :class:`.CheckDetail` objects to construct your result (or results), and then generate
  the TAP output with :func:`~.check.export_TAP`. Read more in :ref:`writing-tasks-for-taskotron`.

  .. _TAP13 format: http://testanything.org/tap-version-13-specification.html
parameters:
  tap_last:
    required: true
    description: TAP output, last outcome is used. ``tap_worst`` cannot be specified together with
      this one.
    type: str
  tap_worst:
    required: true
    description: TAP output, worst outcome is used. ``tap_last`` cannot be specified together with
      this one.
    type: str
returns: |
  :int: Returncode based on TAP last or worst outcome. Success is ``0``, failure is ``100``.
raises: |
  * :class:`.TaskotronDirectiveError`: when there's not exactly one of parameters ``tap_last`` or
    ``tap_last`` present
  * :class:`.TaskotronValueError`: when TAP input cannot be parsed
version_added: 0.3.18
"""

EXAMPLES = """
Run a check returning multiple results and make ``runtask`` fail if any of the results is failed::

  - name: run my check and return TAP
    python:
        file: my_check.py
        callable: run
    export: tap

  - name: set runtask exit code according to the worst result in TAP
    exitcode:
        tap_worst: ${tap}

Run a check that returns multiple results and one "overall" result (at the end) which is computed
according check's internal logic. Then make ``runtask`` fail if the overall result failed::

  - name: run my check and return TAP
    python:
        file: my_check.py
        callable: run
    export: tap

  - name: set runtask exit code according to the last (overall) result in TAP
    exitcode:
        tap_last: ${tap}
"""

# exitcode for PASSED and INFO outcome
SUCCESS = 0

# exitcode for other outcomes
FAILURE = 100

directive_class = 'ExitcodeDirective'


class ExitcodeDirective(BaseDirective):
    def process(self, input_data, env_data):
        """Process this directive.
        :param dict input_data: dictionary of all kwargs from yaml declaration
        :param dict env_data: information on the environment in which we're running
        :returns: returncode based on TAP last or worst outcome
        :raise TaskotronDirectiveError: ``input data`` doesn't contain exactly one of keys
                                        ``tap_last`` or ``tap_last``
        :raise TaskotronValueError: when TAP input cannot be parsed
        """

        # keys 'tap_worst' and 'tap_last' must be mutually exclusive
        if ('tap_worst' in input_data) == ('tap_last' in input_data):
            raise TaskotronDirectiveError("The exitcode directive requires exactly one of keys "
                                          "'tap_worst' or 'tap_last'.")

        if 'tap_worst' in input_data:
            details = check.import_TAP(input_data['tap_worst'])
            code = SUCCESS
            for detail in details:
                if detail.outcome not in ['PASSED', 'INFO']:
                    code = FAILURE

            log.debug("Returning exitcode %d" % code)
            return code

        elif 'tap_last' in input_data:
            details = check.import_TAP(input_data['tap_last'])
            if details and details[-1].outcome not in ['PASSED', 'INFO']:

                log.debug("Returning exitcode %d" % FAILURE)
                return FAILURE
            else:
                log.debug("Returning exitcode %d" % SUCCESS)
                return SUCCESS

        else:
            assert False, "This should never occur"

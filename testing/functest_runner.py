# -*- coding: utf-8 -*-
# Copyright 2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

import sys
import pytest
import logging

from libtaskotron import runner
from libtaskotron import logger

class TestRunnerMain():
    '''Test main()'''

    def test_debug(self, tmpdir, monkeypatch, capfd):
        '''Test whether --debug cmdline option works'''
        fake_recipe_path = tmpdir.join('nonexistent.yml').strpath
        fake_cmdline = ['runner.py', '--debug', fake_recipe_path]
        monkeypatch.setattr(sys, 'argv', fake_cmdline)

        with pytest.raises(IOError):
            # an error should be raised, because the yaml file does not exist
            runner.main()

        # even though the error was raised, the debug level should already be set
        assert logger.stream_handler.level == logging.DEBUG

        # and debug messages should be captured
        msg = "testing debug logging with --debug cmdline option"
        out, err = capfd.readouterr()
        logger.log.debug(msg)
        out, err = capfd.readouterr()
        assert msg in err

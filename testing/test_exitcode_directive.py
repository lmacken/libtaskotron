# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

from libtaskotron.directives import exitcode_directive
from libtaskotron.exceptions import TaskotronDirectiveError, TaskotronValueError
import pytest


@pytest.mark.usefixtures("setup")
class TestReturncode(object):

    @pytest.fixture
    def setup(self):
        self.test_env_data = {}
        self.test_input_data = {}
        self.test_tap = """
TAP version 13
1..4
ok - $CHECKNAME for Hello World P
  ---
  item: Hello World P
  outcome: PASSED
  ...
ok - $CHECKNAME for Hello World I
  ---
  item: Hello World I
  outcome: INFO
  ...
not ok - $CHECKNAME for Hello World F	# FAIL
  ---
  item: Hello World F
  outcome: FAILED
  ...
ok - $CHECKNAME for Hello World P
  ---
  item: Hello World P
  outcome: PASSED
  ...
"""

    def test_tap_last(self):
        self.test_input_data['tap_last'] = self.test_tap
        exitcode = exitcode_directive.ExitcodeDirective().process(self.test_input_data,
                                                                  self.test_env_data)
        assert exitcode == exitcode_directive.SUCCESS

    def test_tap_worst(self):
        self.test_input_data['tap_worst'] = self.test_tap
        exitcode = exitcode_directive.ExitcodeDirective().process(self.test_input_data,
                                                                  self.test_env_data)
        assert exitcode == exitcode_directive.FAILURE

    def test_bad_input(self):
        with pytest.raises(TaskotronDirectiveError):
            exitcode_directive.ExitcodeDirective().process(self.test_input_data,
                                                           self.test_env_data)

    def test_bad_tap(self):
        self.test_tap = "noworky"
        self.test_input_data['tap_worst'] = self.test_tap
        with pytest.raises(TaskotronValueError):
            exitcode_directive.ExitcodeDirective().process(self.test_input_data,
                                                           self.test_env_data)

    def test_empty_tap_worst(self):
        self.test_tap = """
TAP version 13
1..0
"""
        self.test_input_data['tap_worst'] = self.test_tap
        exitcode = exitcode_directive.ExitcodeDirective().process(self.test_input_data,
                                                                  self.test_env_data)
        assert exitcode == exitcode_directive.SUCCESS

    def test_empty_tap_last(self):
        self.test_tap = """
TAP version 13
1..0
"""
        self.test_input_data['tap_last'] = self.test_tap
        exitcode = exitcode_directive.ExitcodeDirective().process(self.test_input_data,
                                                                  self.test_env_data)
        assert exitcode == exitcode_directive.SUCCESS

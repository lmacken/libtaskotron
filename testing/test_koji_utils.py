# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

'''Unit tests for libtaskotron/koji_utils.py'''

import pytest
from dingus import Dingus
import os
import itertools

from libtaskotron import koji_utils
from libtaskotron import exceptions as exc
from libtaskotron import config
from libtaskotron import file_utils

from test_file_utils import mock_download

# http://stackoverflow.com/questions/3190706/nonlocal-keyword-in-python-2-x
def create_multicall(first, second):
    first_call = {"value": True}

    def multicall():
        if first_call["value"]:
            first_call["value"] = False
            return first
        else:
            return second

    return multicall


class TestKojiClient():
    def setup_method(self, method):
        self.ref_nvr = 'foo-1.2-3.fc99'
        self.ref_latest_stable = 'foo-1.2-2.fc99'
        self.ref_arch = 'noarch'
        self.ref_name = 'foo'
        self.ref_version = '1.2'
        self.ref_release = '3.fc99'
        self.ref_buildid = 123456
        self.ref_filename = "%s.%s.rpm" % (self.ref_nvr, self.ref_arch)
        self.ref_tags = ['f99-updates', 'f99']

        self.ref_build = {'package_name': self.ref_name,
                          'version': self.ref_version,
                          'release': self.ref_release,
                          'id': self.ref_buildid,
                          'nvr': '%s-%s-%s' % (self.ref_name, self.ref_version,
                                               self.ref_release)}

        self.ref_rpms = [{'name': self.ref_name, 'version': self.ref_version,
                          'release': self.ref_release, 'nvr': self.ref_nvr,
                          'arch': self.ref_arch, 'build_id': self.ref_buildid,},
                    {'name': self.ref_name, 'version': self.ref_version,
                     'release': self.ref_release, 'nvr': self.ref_nvr,
                     'arch': 'src', 'build_id': self.ref_buildid, },
                    {'name': self.ref_name + '-debuginfo',
                     'version': self.ref_version,
                     'release': self.ref_release, 'nvr': self.ref_nvr,
                     'arch': 'src', 'build_id': self.ref_buildid, },
                    {'name': self.ref_name + '-debuginfo-common',
                     'version': self.ref_version,
                     'release': self.ref_release, 'nvr': self.ref_nvr,
                     'arch': 'src', 'build_id': self.ref_buildid, }]

    def test_latest_by_tag_first_tag_miss(self):
        stub_koji = Dingus(listTagged__returns=None,
                           multiCall__returns=[[[]], [[{'nvr': self.ref_latest_stable}]]])

        test_koji = koji_utils.KojiClient(stub_koji)
        outcome = test_koji.latest_by_tag(self.ref_tags, self.ref_name)

        assert outcome == self.ref_latest_stable

    def test_latest_by_tag_second_tag_miss(self):
        stub_koji = Dingus(listTagged__returns=None,
                           multiCall__returns=[[[{'nvr': self.ref_latest_stable}]], [[]]])

        test_koji = koji_utils.KojiClient(stub_koji)
        outcome = test_koji.latest_by_tag(self.ref_tags, self.ref_name)

        assert outcome == self.ref_latest_stable

    def test_latest_by_tag_build_not_found(self):
        stub_koji = Dingus(listTagged__returns=None,
                           multiCall__returns=[[[]], [[]]])

        test_koji = koji_utils.KojiClient(stub_koji)
        assert test_koji.latest_by_tag(self.ref_tags, self.ref_name) is None

    def test_rpms_to_build(self):
        stub_koji = Dingus(getBuild__returns=None,
                           getRPM__returns=None,
                           multiCall=create_multicall(
                               [[self.ref_rpms[0]], [self.ref_rpms[1]]],
                               [[self.ref_build], [self.ref_build]]))

        test_koji = koji_utils.KojiClient(stub_koji)
        outcome = test_koji.rpms_to_build([self.ref_filename,self.ref_filename])

        assert outcome == [self.ref_build, self.ref_build]
        # because two rpms come from same build, it gets called twice for each
        # rpm, once for build
        assert len(stub_koji.calls) == 3


    def test_rpms_to_build_exceptions(self):
        stub_koji = Dingus(getRPM__returns=None,
                           multiCall__returns=[{"faultCode": -1,
                                                "faultString": "failed"}])
        test_koji = koji_utils.KojiClient(stub_koji)

        with pytest.raises(exc.TaskotronRemoteError):
            test_koji.rpms_to_build([self.ref_filename])

        stub_koji = Dingus(getBuild__returns=None,
                           getRPM__returns=None,
                           multiCall=create_multicall([[self.ref_rpms[0]]], [
                               {"faultCode": -1, "faultString": "failed"}]))

        test_koji = koji_utils.KojiClient(stub_koji)
        with pytest.raises(exc.TaskotronRemoteError):
            test_koji.rpms_to_build([self.ref_filename])

        stub_koji = Dingus(getBuild__returns=None,
                           getRPM__returns=None,
                           multiCall__returns=[[None]])

        test_koji = koji_utils.KojiClient(stub_koji)
        with pytest.raises(exc.TaskotronRemoteError):
            test_koji.rpms_to_build([self.ref_filename])

        stub_koji = Dingus(getBuild__returns=None,
                           getRPM__returns=None,
                           multiCall=create_multicall([[self.ref_rpms[0]]],
                                                      [[None]]))

        test_koji = koji_utils.KojiClient(stub_koji)
        with pytest.raises(exc.TaskotronRemoteError):
            test_koji.rpms_to_build([self.ref_filename])


    def test_get_noarch_rpmurls_from_nvr(self):
        stub_koji = Dingus(getBuild__returns = self.ref_build,
                           listRPMs__returns = self.ref_rpms)

        test_koji = koji_utils.KojiClient(stub_koji)

        test_urls = test_koji.nvr_to_urls(self.ref_nvr)

        koji_baseurl = config.get_config().pkg_url
        ref_urls = ['%s/%s/%s/%s/%s/%s.%s.rpm' % (koji_baseurl, self.ref_name,
                        self.ref_version, self.ref_release, self.ref_arch,
                        self.ref_nvr, self.ref_arch),
                    '%s/%s/%s/%s/src/%s.src.rpm' % (koji_baseurl, self.ref_name,
                        self.ref_version, self.ref_release, self.ref_nvr),
                    ]

        assert test_urls == ref_urls

    def should_i386_rpmurls_query_i686(self):
        self.ref_arch = 'i386'

        stub_koji = Dingus(getBuild__returns = self.ref_build)

        test_koji = koji_utils.KojiClient(stub_koji)

        # this isn't setup to actually return urls since we don't care about
        # that for this test
        # it should throw an exception in this case.
        try:
            test_koji.nvr_to_urls(self.ref_nvr, arches = [self.ref_arch])
        except exc.TaskotronRemoteError:
            pass

        listrpm_calls = stub_koji.calls('listRPMs')
        requested_arches = listrpm_calls[0][2]['arches']

        assert 'i686' in requested_arches
        assert 'i386' in requested_arches

    def should_not_throw_exception_norpms(self):
        '''It's possible to have no RPMs (for the given arch) in a build'''
        stub_koji = Dingus(getBuild__returns = self.ref_build)
        test_koji = koji_utils.KojiClient(stub_koji)

        test_koji.nvr_to_urls(self.ref_nvr, arches = [self.ref_arch])

    def test_nvr_to_urls_src_filtering(self):
        stub_koji = Dingus(getBuild__returns=self.ref_build,
                           listRPMs__returns=self.ref_rpms)
        test_koji = koji_utils.KojiClient(stub_koji)

        # arch and src enabled
        urls = test_koji.nvr_to_urls(self.ref_nvr, arches=[self.ref_arch])
        assert len(urls) == 2
        assert 'src' in urls[0] or 'src' in urls[1]

        # arch enabled, src disabled
        urls = test_koji.nvr_to_urls(self.ref_nvr, arches=[self.ref_arch],
                                     src=False)
        assert len(urls) == 1
        assert 'src' not in urls[0]

    def test_nvr_to_urls_debuginfo(self):
        stub_koji = Dingus(getBuild__returns=self.ref_build,
                           listRPMs__returns=self.ref_rpms)
        test_koji = koji_utils.KojiClient(stub_koji)

        # arch enabled, src enabled, debuginfo enabled
        urls = test_koji.nvr_to_urls(self.ref_nvr, arches=[self.ref_arch],
                                     debuginfo=True)
        assert len(urls) == 4
        assert len([url for url in urls if '-debuginfo' in url]) == 2

    def test_nvr_to_urls_only_src(self):
        stub_koji = Dingus(getBuild__returns=self.ref_build)
        test_koji = koji_utils.KojiClient(stub_koji)

        # arch disabled, src enabled
        test_koji.nvr_to_urls(self.ref_nvr, arches=[], src=True)
        listrpm_calls = stub_koji.calls('listRPMs')
        requested_arches = listrpm_calls[0][2]['arches']
        assert len(requested_arches) == 1
        assert requested_arches[0] == 'src'

    def should_raise_no_arch_no_src(self):
        test_koji = koji_utils.KojiClient(Dingus())

        with pytest.raises(exc.TaskotronValueError):
            test_koji.nvr_to_urls(self.ref_nvr, arches=[], src=False)


    # === get_nvr_rpms ===

    def test_get_nvr_rpms_simple(self, monkeypatch):
        '''NVR contains a few RPMs'''
        test_koji = koji_utils.KojiClient(Dingus())

        stub_urls = [
            'http://localhost/file1.rpm',
            'http://localhost/file2.rpm']
        monkeypatch.setattr(test_koji, 'nvr_to_urls',
                            lambda *args, **kwargs: stub_urls)
        monkeypatch.setattr(file_utils, 'download', mock_download)
        monkeypatch.setattr(file_utils, 'makedirs', Dingus())

        rpmdir = '/fake'
        rpm_files = test_koji.get_nvr_rpms(self.ref_nvr, dest=rpmdir)

        assert rpm_files == [
            os.path.join(rpmdir, 'file1.rpm'),
            os.path.join(rpmdir, 'file2.rpm')]

    def test_get_nvr_rpms_empty(self, monkeypatch):
        '''NVR contains no RPMs (e.g. of a particular arch)'''
        test_koji = koji_utils.KojiClient(Dingus())

        monkeypatch.setattr(test_koji, 'nvr_to_urls', lambda *args, **kwargs: [])
        monkeypatch.setattr(file_utils, 'download', mock_download)
        monkeypatch.setattr(file_utils, 'makedirs', Dingus())

        rpmdir = '/fake'
        rpm_files = test_koji.get_nvr_rpms(self.ref_nvr, dest=rpmdir)

        assert rpm_files == []

    def test_get_nvr_rpms_production_profile(self, monkeypatch):
        '''caching should be disabled in production profile'''
        test_koji = koji_utils.KojiClient(Dingus())
        stub_download = Dingus()

        monkeypatch.setattr(test_koji, 'nvr_to_urls',
                            lambda *args, **kwargs: ['foo'])
        monkeypatch.setattr(file_utils, 'download', stub_download)
        monkeypatch.setattr(file_utils, 'makedirs', Dingus())
        monkeypatch.setattr(config, '_config', config.ProductionConfig)

        rpmdir = '/fake'
        test_koji.get_nvr_rpms(self.ref_nvr, dest=rpmdir)

        call = stub_download.calls[0]
        assert call[2]['cachedir'] is None

    def test_get_nvr_rpms_development_profile(self, monkeypatch):
        '''caching should be disabled in development profile'''
        test_koji = koji_utils.KojiClient(Dingus())
        stub_download = Dingus()

        monkeypatch.setattr(test_koji, 'nvr_to_urls',
                            lambda *args, **kwargs: ['foo'])
        monkeypatch.setattr(file_utils, 'download', stub_download)
        monkeypatch.setattr(file_utils, 'makedirs', Dingus())
        monkeypatch.setattr(config, '_config', config.Config)

        rpmdir = '/fake'
        test_koji.get_nvr_rpms(self.ref_nvr, dest=rpmdir)

        call = stub_download.calls[0]
        assert call[2]['cachedir'] is not None


    # === get_tagged_rpms ===

    def test_get_tagged_rpms_single(self, monkeypatch):
        '''Single NVR in a tag'''
        stub_koji = Dingus(listTagged__returns=[self.ref_build])
        test_koji = koji_utils.KojiClient(stub_koji)

        stub_rpms = [
            '/fake/file1.rpm',
            '/fake/file2.rpm']
        monkeypatch.setattr(test_koji, 'get_nvr_rpms',
                            lambda *args, **kwargs: stub_rpms)
        monkeypatch.setattr(file_utils, 'makedirs', lambda *args, **kwargs: None)

        rpm_files = test_koji.get_tagged_rpms('some tag', dest=None)

        assert rpm_files == stub_rpms

    def test_get_tagged_rpms_multiple(self, monkeypatch):
        '''Multiple NVRs in a tag'''
        stub_koji = Dingus(listTagged__returns=[self.ref_build,
                                                {'nvr': 'bar-1-1'}])
        test_koji = koji_utils.KojiClient(stub_koji)

        stub_rpms = {self.ref_build['nvr']: ['/fake/file1.rpm',
                                             '/fake/file2.rpm'],
                     'bar-1-1': ['/fake/bar1.rpm']
                    }
        monkeypatch.setattr(test_koji, 'get_nvr_rpms',
                            lambda nvr, *args, **kwargs: stub_rpms[nvr])
        monkeypatch.setattr(file_utils, 'makedirs', lambda *args, **kwargs: None)

        rpm_files = test_koji.get_tagged_rpms('some tag', dest=None)

        assert sorted(rpm_files) == sorted(itertools.chain(*stub_rpms.values()))

    def test_get_tagged_rpms_none(self, monkeypatch):
        '''No NVRs in a tag'''
        stub_koji = Dingus(listTagged__returns=[])
        test_koji = koji_utils.KojiClient(stub_koji)
        stub_get_nvr_rpms = Dingus()

        monkeypatch.setattr(test_koji, 'get_nvr_rpms', stub_get_nvr_rpms)
        monkeypatch.setattr(file_utils, 'makedirs', lambda *args, **kwargs: None)

        rpm_files = test_koji.get_tagged_rpms('some tag', dest=None)

        assert rpm_files == []
        # get_nvr_rpms() should not be called
        assert stub_get_nvr_rpms.calls() == []


class TestGetENVR(object):

    def test_epoch(self):
        '''Epoch included in build'''
        build = {'nvr': 'foo-1.2-3.fc20',
                 'epoch': 1,
                }
        assert koji_utils.getNEVR(build) == 'foo-1:1.2-3.fc20'

    def test_no_epoch(self):
        '''Epoch not included in build'''
        build = {'nvr': 'foo-1.2-3.fc20',
                 'epoch': None,
                }
        assert koji_utils.getNEVR(build) == 'foo-1.2-3.fc20'

    def test_raise(self):
        '''Invalid input param'''
        with pytest.raises(exc.TaskotronValueError):
            koji_utils.getNEVR('foo-1.2-3.fc20')

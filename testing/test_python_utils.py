# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

'''Unit tests for libtaskotron/python_utils.py'''

import pytest

from libtaskotron import python_utils
import libtaskotron.exceptions as exc

class TestCollectionOf:
    '''This basically tests `iterable` and `sequence`.'''

    def test_iterable(self):
        assert python_utils.iterable([1, 2])
        assert python_utils.iterable(['a', 'b'])
        assert python_utils.iterable(('foo',))
        assert python_utils.iterable({'foo', 'bar', 'baz'})
        assert python_utils.iterable(set())
        assert python_utils.iterable(())
        assert python_utils.iterable([u'foo', u'bar'])
        assert python_utils.iterable({'a': 1, 'b': 2})

    def test_not_iterable(self):
        assert not python_utils.iterable('a')
        assert not python_utils.iterable(u'a')
        assert not python_utils.iterable(unicode('foo'))
        assert not python_utils.iterable(3)
        assert not python_utils.iterable(3.14)
        assert not python_utils.iterable(None)
        assert not python_utils.iterable(object())

    def test_iterable_items(self):
        assert python_utils.iterable([1, 2], int)
        assert not python_utils.iterable([1, 2], float)
        assert not python_utils.iterable([1, 2.2], float)

        assert python_utils.iterable(['a', 'b'], str)
        assert python_utils.iterable(['a', 'b'], basestring)
        assert not python_utils.iterable(['a', 'b'], unicode)

        assert python_utils.iterable([[],[]], list)

        # empty classes
        X = type('X', (object,), {})
        Y = type('Y', (X,), {})
        assert python_utils.iterable((X(), X()), X)
        assert python_utils.iterable((X(), Y()), X)
        assert not python_utils.iterable((X(), Y()), Y)

    def test_raise(self):
        with pytest.raises(exc.TaskotronValueError):
            assert python_utils.iterable([1, 2], 1)

        with pytest.raises(exc.TaskotronValueError):
            assert python_utils.iterable([1, 2], 'a')

        with pytest.raises(exc.TaskotronValueError):
            assert python_utils.iterable([1, 2], [])

        X = type('X', (object,), {})
        with pytest.raises(exc.TaskotronValueError):
            assert python_utils.iterable([1, 2], X())

    def test_sequence(self):
        assert python_utils.sequence([1, 2])
        assert python_utils.sequence(['a', 'b'])
        assert python_utils.sequence(('foo',))
        assert python_utils.sequence(())
        assert python_utils.sequence([u'foo', u'bar'])

    def test_not_sequence(self):
        assert not python_utils.sequence({'foo', 'bar', 'baz'})
        assert not python_utils.sequence(set())
        assert not python_utils.sequence({'a': 1, 'b': 2})
